/**
 * See (http://jquery.com/). 
 * @name jQuery
 * @class
 * See the jQuery Library  (http://jquery.com/) for full details.  This just
 * documents the function and classes that are added to jQuery by this plug-in.
 */

/**
 * See (http://jquery.com/).
 * @name $
 * @class
 * See the jQuery Library  (http://jquery.com/) for full details.  This just
 * documents the function and classes that are added to jQuery by this plug-in.
 */

/**
 * See (http://jquery.com/).
 * @name fn
 * @class
 * See the jQuery Library  (http://jquery.com/) for full details.  This just
 * documents the function and classes that are added to jQuery by this plug-in.
 * @memberOf jQuery
 */

// require.config --> shim: ["jquery"]

/**
 * @ignore
 */
$.fn.genericSelect2 = function(options) {
	var $this = $(this);
	var obj = new $.genericSelect2($this, options);
	obj.data("genericSelect2", obj);

	return obj;
};

/**
 * genericSelect2 plug-in 
 * @class
 * @constructor 
 * @author "<a href="mailto:sperrone@gmail.com.ar">Sebasti&aacute;n Perrone (sperrone@gmail.com.ar)</a>"
 * @memberOf $
 */
$.genericSelect2 = function($this, options) {

	$this.options = $.extend({}, $.fn.genericSelect2.defaults, options); 
	var o = $this.options;
	var l = $.extend({}, $.fn.genericSelect2.labels[o.language]); 
	$this.addClass("generic-select2");

	/**
	 * Returns the plug-in name
	 * @returns {String} The plug-in name
	 * @name pluginName
	 * @public 
	 * @inner
	 * @function 
	 * @memberOf $.genericSelect2
	 */
	$this.pluginName = function() {
		return "genericSelect2";
	};
	$this.version = "1.0.0";
	$this.pluginUri = "/generic-select2-" + $this.version;

	/**
	 * Inicializacion de un plugin que puede ser padre
	 */
	function init() {
		var htmlToLoad;
		$this.autocompleteApplied = false;
		$this.autocompleteOptions = [];
		if (o.logging) console.debug($this.pluginName() + " - options: " + JSON.stringify(o, null, 2));
		if (o.logging) console.debug($this.pluginName() + " - labels: " + JSON.stringify(l, null, 2));
		if (o.orientation == "h") {
			htmlToLoad = "/generic-select2-h.html";
		} else {
			htmlToLoad = "/generic-select2-v.html";
		}
		$this.load(o.pluginsUrl + $this.pluginUri + htmlToLoad, function() {
			$("head").append($("<link>", {href: o.pluginsUrl + $this.pluginUri + "/generic-select2.css", rel: "stylesheet"}));
			$("head").append($("<link>", {href: o.contextPath + "/js/select2-4.0.3/select2.css", rel: "stylesheet"}));

			// inicializacion de propiedades
			$this.opened = false;
			
			// asignacion de campos y etiquetas
			$this.autocompletoContainer = $this.find(".autocompleto-select2-container");
			$this.labelAutocompleto = $this.find(".label-autocompleto-select2");
			$this.autocompleto = $this.find(".autocompleto-select2");
			$this.autocompletoDesc = $this.find(".autocompleto-select2-desc");
			
			// aplicacion / traduccion de etiquetas
			$this.labelAutocompleto.html(o.label);

			// formateo horizontal / vertical
			if (o.orientation == "h") {
				$this.autocompletoContainer.addClass("col-sm-" + o.fieldWidth);
				$this.labelAutocompleto.addClass("col-sm-" + o.labelWidth);
			}
			
			$this.autocompleto.attr("name", o.nameInForm);
			$this.autocompletoDesc.attr("name", o.descNameInForm);
			
			// captura de eventos
			$this.autocompleto.bind("change", handleAutocompletoChange);
			$this.autocompleto.bind("select2:selecting", handleAutocompletoSelect);
			$this.autocompleto.bind("select2:unselecting", handleAutocompletoUnselecting);

			// aplicacion de plugin select2
			createSelect2();
			
			// emision de evento de initilizacion finalizada
			$this.trigger({
				type: "genericSelect2.initialized",
				message: "DGA Autocompleto Select2 Initialized",
				time: new Date()
			});
		});
	}
	
	
	/**
	 * Solicita la carga de datos
	 */
	$this.loadData = function(pData) {
		setTimeout(function() {
			var url = o.contextPath + o.servicesUri + o.loadDataService;
			
			var dataType = "json";
			
			$.ajax({
				type: o.method, 
				url: url,
				data: pData,
				success: responseLoad,
				dataType: dataType
			});
		}, o.forcedDelay);
	};
	
	
	$this.loadFromArray = function(pArray) {
		var emulatedResponse = {
			status: "OK",
			data: pArray
		};
		responseLoad(emulatedResponse);
	};

	
	/**
	 * Atiende la respuesta de la solicitud de datos 
	 * @param {Object} json JSON con los datos
	 */
	function responseLoad(json) {
		json = o.transformationInfoFunc(json);
		
		$this.autocompleteOptions = arrayObjectReplaceKeys(json.data, [o.dataKey, o.valueKey], ["id", "text"]);
		$this.listOptions = transformToObjectIndexedBy(json.data, o.dataKey, [o.valueKey]);
		$this.completeListOptions = transformToIndexedBy(json.data, o.dataKey); 
		$this.invListOptions = transformToObjectIndexedBy(json.data, o.valueKey, o.dataKey);

		createSelect2();
		
		$this.clear();

		$this.trigger({
			type: "genericSelect2.dataLoaded",
			message: "DGA Autocompleto Select2 Loaded",
			time: new Date()
		});
		
	}

	
	function createSelect2() {
		$this.autocompleteApplied = true;
		$this.autocompleto.select2().empty();
		$this.autocompleto.select2({
			data: $this.autocompleteOptions,
			placeholder: o.placeholder,
		    allowClear: o.allowClear,
		    multiple: o.multiple
		});

		$this.find("span.twitter-typeahead").css("display", "inherit");
	}
	
	/**
	 * Maneja el evento de modificacion de datos del campo (se dispara cuando se cambio el valor y se pierde el foco del campo) 
	 * @param {Object} ev El evento typeahead:change que dispara la invocación
	 */
	function handleAutocompletoChange(ev) {
		var selected_element = $(ev.currentTarget);
	    var select_val = selected_element.val();
	    var desc = null;
	    if (select_val) {
		    if ($this.options.multiple) {
			    if (select_val.length > 0) {
			    	desc = "";
			    	for (var i = 0; i < select_val.length; i++) {
			    		var so = select_val[i];
			    		if (i > 0) {
			    			desc = desc + ", ";
			    		}
			    		desc = desc + $this.listOptions[so][$this.options.valueKey]; 
			    	}
				    $this.autocompletoDesc.val(desc);
			    }
		    } else {
			    if (select_val) {
				    desc = $this.listOptions[select_val][$this.options.valueKey];
				    $this.autocompletoDesc.val(desc);
			    }
		    }
			$this.trigger({
				type: "genericSelect2.change",
				message: "DGA Autocompleto Select2 Change",
				time: new Date(),
				selectedOption: select_val,
				selectedDescription: desc
			});
	    }
	}
	
	
	function handleAutocompletoUnselecting(ev) {
		$this.autocompleto.select2("close");
		$this.trigger({
			type: "genericSelect2.clear",
			message: "DGA Autocompleto Select2 clear",
			time: new Date()
		});
	}
	

	/**
	 * Maneja el evento de seleccion de un dato en el campo  
	 * @param {Object} ev El evento typeahead:select que dispara la invocación
	 */
	function handleAutocompletoSelect(ev, pValue) {
		var selected_element = $(ev.currentTarget);
	    var select_val = selected_element.val();
		$this.trigger({
			type: "genericSelect2.select",
			message: "DGA Autocompleto Select2 Select",
			initSelection: function(element, callback) {},
			time: new Date(),
			selectedOption: select_val
		});
	}

	
	/**
	 * Establece u obtiene el valor del campo
	 * @param {Number} arg1 El codigo del dato a establecer (solo funciona para establecer el valor)
	 * @returns 
	 */
	$this.val = function() {
		if (arguments.length == 0) {
			var k = $this.autocompleto.select2("val");
			var fv = $this.listOptions[k];
			if (k) {
				r = {
					key: k,
					value: fv
				};
			} else {
				r = null;
			}
			return r;
		} else {
			var k = arguments[0];
			if (k) {
				$this.autocompleto.select2("val", [k]);
			} else {
				$this.autocompleto.select2("val", [""]);
			}
		}
	};
	
	
	$this.fullVal = function() {
		var v = $this.val();
		var r = $this.completeListOptions[v.key];
		
		return r;
	};
	
	
	$this.clear = function() {
		$this.val(null);
	};


	$this.setWithError = function() {
		$this.autocompleto.addClass("error");
	};
	
	$this.disable = function() {
		$this.autocompleto.addClass("disabled");
		$this.autocompleto.select2("enable", false);
		$this.autocompleto.prop("disabled", true);
	};

	
	$this.enable = function() {
		$this.autocompleto.removeClass("disabled");
		$this.autocompleto.select2("enable", true);
		$this.autocompleto.prop("disabled", false);
	};
	
	
	$this.linearVal = function() {
		return $this.val().key;
	};
	
	
	/**
	 * Set an option with the given value
	 * @param {String} pOption The name of the options to change
	 * @param {Object} pValue The value to set into the option
	 */
	$this.setOption = function(pOption, pValue) {
		$this.options[pOption] = pValue;
		o[pOption] = pValue;
		$this.parent.setOption(pOption, pValue);
	};
	
	init();

	return $this;
};


/**
 * genericSelect2 defaults  
 * @name defaults
 * @memberOf $.genericSelect2
 * @class
 * @property {String} language El lenguaje por omision (codigo ISO)
 * @property {Boolean} logging Si es true emite log en la consola 
 * @property {String} backendUrl La URL para a partir de donde se desprenden los servicios
 * @property {String} contextPath El path de contexto del sitio (p.ej: http://algunsitio.com)
 * @property {String} pluginsUrl La URL principal en donde estan los plugins  
 * @property {String} pluginsUri La URI en donde estan los plugins  
 * @property {String} pagesUrl La URL principal en donde se localizan las paginas  
 * @property {String} pagesUri La URI en donde se localizan las paginas  
 * @property {String} imagesUrl La URL principal en donde estan las imagenes  
 * @property {String} imagesUri La URI en donde estan las imagenes
 * @property {String} cssUrl La URL en donde estan los archivos de estilo CSS 
 * @property {String} cssUri La URI en donde estan los archivos de estilo CSS
 * -------------------------------------------------------------------------------
 */
$.fn.genericSelect2.defaults = {
	language: "en",
	logging: true,
	contextPath: "/",
	pluginsUri: "/",
	pluginsUrl: "/",
	pagesUrl: "/",
	pagesUri: "/",
	imagesUrl: "/",
	imagesUri: "/",
	cssUrl: "/",
	cssUri: "/",
	dataKey: "code",
	valueKey: "name",
	name: "candy",
	label: "[label]",
	placeholder: "- seleccione -",
	loadDataService: "/loadCandies.json",
	data: {},
	method: "GET",
	allowClear: true,
	multiple: false,
	forcedDelay: 0,
	orientation: "v", // v: vertical, h: horizontal
	labelWidth: 3,
	fieldWidth: 9,
	transformationInfoFunc: function(json) {return json;},
	nameInForm: "field",
	descNameInForm: "descField"
};


/**
 * genericSelect2 labels
 * @name labels
 * @memberOf $.genericSelect2
 */
$.fn.genericSelect2.labels = {
	en: {
	},
	es: {
	}
};
