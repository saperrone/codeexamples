
var logoOriginalSize = 256;

// pre-ajax
$.ajaxSetup({
	cache: false,
	beforeSend: function(xhr) {
		var tkn = document.querySelector("meta[name="+ defaults.token +"]");
		if(tkn) {
			xhr.setRequestHeader(defaults.token, tkn.content);
		}
	}
});

$(document).ready(function(e){
	$("body").popover({
		selector: "[data-toggle='popover']",
		placement: "top",
		trigger: "focus",
		title: ""
	});
});


/**
 * Llena un dropdown dado un array de opciones, seteando valor y etiqueta
 * @param {jQueryObject} combo Un objeto jQuery que contenga un dropdown 
 * @param {ArrayObject} options Un array de objetos
 * @param {String} key Nombre de la propiedad que contiene la clave 
 * @param {String} label Nombre de la propiedad que contiene la etiqueta
 */
function fillSelectFromJson(combo, options, key, label, placeholder) {
	var sel, oldval, keyVal, labelVal;
	
	oldval = combo.val();
	combo.empty();
	if (placeholder) {
		combo.append($("<option>", {value: "%"}).addClass("dropdownPlaceholder").text(placeholder));
	}
	for (var i = 0; i < options.length; i++) {
		keyVal = options[i][key];
		labelVal = "";
		
		if(Array.isArray(label)) {
			for(var j = 0; j < label.length; j++) {
				var part = label[j];
				labelVal += (options[i][part] + " - ");
			}
			labelVal = labelVal.substring(0, labelVal.length - 3)
		} else
			labelVal = options[i][label];
		
		sel = $("<option>", {value: keyVal}).html(labelVal);
		combo.append(sel);
	}
	if (oldval) {
		combo.val(oldval);
	}
	if (!combo.val()) {
		combo.val("%");
	}
	
	if (combo.selectmenu) combo.selectmenu("refresh", true);
}


function isOrientationPortrait(){
	if ($(window).height() > $(window).width()){ 
		return true; 
	} else { 
		return false; 
	}
}


/**
 * Show errors in application
 * @param {Object} a1 An object with error structure 
 * @param {Object} a2 An object with error structure
 * @param {Object} a3 An object with error structure
 */
function showErrors(a1, a2, a3) {
	if (a1.responseText) {
		text = a1.responseText;
	} else if (a2 && a3) {
		text = a2 + ": " + a3.message;
	} else {
		var text;
		if (a2) {
			text = a2.code + '-' + a2.message;
		} else {
			text = a1;
		}
	}
	console.log(text);
	console.log(JSON.stringify(text));
	alert(text);
}


/**
 * Clears all local data
 */
function clearLocalData() {
	localStorage.removeItem("twb");
}

/**
 * Process an AJAX response error (using DJANGO)
 */
function processAjaxError(jqXHR) {
	var field, ret, fieldErrors, field2, fieldErrors2;
	ret = "";
	if (jqXHR.responseJSON) {
		for (field in jqXHR.responseJSON) {
			fieldErrors = jqXHR.responseJSON[field];
			if (field == "non_field_errors") {
				ret = ret + "<br>" + fieldErrors.join("<br>");
			} else {
				if ($.isPlainObject(fieldErrors)) {
					for (field2 in fieldErrors) {
						fieldErrors2 = fieldErrors[field2];
						ret = ret + "<br>" + field2 + ": " + fieldErrors2;
					}
				} else {
					ret = ret + "<br>" + field + ": " + fieldErrors.join("<br>");
				}
			}
		}
	} else {
		ret = "Undefined error: " + jqXHR.response;
	}
	return ret;
}


/**
 * Determines if the given string is an email address
 * @param {String} pEmail An string to be validated 
 * @returns {Boolean} True when is a valid email, otherwise false
 */
function emailIsValid(pEmail) {
	if (pEmail) {
		return (pEmail.search(/^[0-9a-zA-Z_\.]	+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/) != -1);
	} else {
		return false;
	}
}


/**
 * Determines if the app runs into the Ripples Emulator
 * @returns {Boolean} True if the app runs into the Ripples Emulator, otherwise false
 */
function ripplesEmulator() {
	return (window.tinyHippos != undefined); 	
}


/**
 * Handle the error given by the paxsus back-end
 * @param json The json returned by the back-end function
 * @param funcHelp The name to concat to the text "there was an error"
 * @param callback The callback function to call if there is no error (passing the json of backend)
 */
function handleError(json, funcHelp, callback) {
	var err;
	if (!funcHelp) {
		funcHelp = "";
	}
	if (json.success) {
		callback(json);
	} else {
		if (json.error_messages) {
			err = "There was an error " + funcHelp + ": <br>" + json.error_messages.join("<br>");
		} else if (json.error) {
			err = "There was an error " + funcHelp + ": <br>" + json.error;
		} else {
			err = "There was an error (non standard format) " + funcHelp + ": <br>" + JSON.stringify(json);
		}
		console.error(err);
		alert(err);
	}
}

/**
 * Genera un formulario en base a un objeto JSON simple y lo submite a la URL especificada
 * TODO: implementar manejo de arrays y objetos anidados
 * @param json El objeto JSON que contiene las propiedades a submitir
 * @param url La URL a la que se debe submitir el formulario
 * @param method El metodo a utilizar para el submit (GET o POST)
 */
function jsonSubmit(json, url, method) {
	var form = document.createElement("form");
	form.method = method;
	form.action = url;
	
	for(var prop in json) {
		var name = prop;
		var value = json[prop];
		
		var input = $("<input>");
		input.attr("type", "text");
		input.attr("name", name);
		input.attr("value", value);
		
		$(form).append(input);
	}
	
	form.submit();
}


/**
 * Funcion para proveer datos JSON a los dropdown autocomplete
 * @param strs
 * @returns {Function}
 */
function substringMatcher(strs) {
		return function findMatches(q, cb) {
		var matches, substringRegex;

	    // an array that will be populated with substring matches
	    matches = [];
	
	    // regex used to determine if a string contains the substring `q`
	    substrRegex = new RegExp(q, 'i');
	
	    // iterate through the pool of strings and for any string that
	    // contains the substring `q`, add it to the `matches` array
	    $.each(strs, function(i, str) {
			if (substrRegex.test(str)) {
			    matches.push(str);
			}
		});
		cb(matches);
		};
};



var typeAheadGeneralClassNames = {
	input: 'typeahead-input',
    hint: 'typeahead-hint',
    selectable: 'typeahead-selectable',
    menu: 'typeahead-menu',
    dataset: 'typeahead-dataset',
    suggestion: 'typeahead-suggestion',
    empty: 'typeahead-empty',
    open: 'typeahead-open',
    cursor: 'typeahead-cursor',
    highlight: 'typeahead-highlight'
};


function validateResponse(pJson) {
	if (pJson.status == "ERROR") {
		popup.show("<p style='color: red;'>Ocurrio un error: " + pJson.errorText + "</p>");
		return false;
	} else {
		return true;
	}
} 

