//TODO: move to a jquery plugin structure

/**
 * Fill a select combo from an array of options, setting the key and the label
 * @param {jQueryObject} combo A jquery select object 
 * @param {ArrayObject} options An array of objects
 * @param {String} key The name of the key entry value in each array element 
 * @param {String} label The name of the value entry in each array element
 */
function fillSelectFromJson(combo, options, key, label, placeholder) {
	var sel, oldval, keyVal, labelVal;
	
	oldval = combo.val();
	combo.empty();
	if (placeholder) {
		combo.append($("<option>", {value: "%"}).addClass("dropdownPlaceholder").text(placeholder));
	}
	for (var i = 0; i < options.length; i++) {
		keyVal = options[i][key];
		labelVal = options[i][label]; 
		sel = $("<option>", {value: keyVal}).html(labelVal);
		combo.append(sel);
	}
	if (oldval) {
		combo.val(oldval);
	}
	if (!combo.val()) {
		combo.val("%");
	}
	
	if (combo.selectmenu) combo.selectmenu("refresh", true);
}


/**
 * Count how many entries have the given object
 * @param {Object} obj The object to evaluate
 * @returns {Number} The quantity of entries that have the object
 */
function entriesCount(obj) {
	var i = 0;
	for (k in obj) {
		i++;
	}
	
	return i;
}


/**
 * Transform an object in an array
 * @param {Object} obj The object to convert to array
 * @returns {Array} An array with each value element in object
 */
function objToArray(obj) {
	var r; 
	r = [];
	for (k in obj) {
		r.push(obj[k]); 
	}
	
	return r;
}

function objToArrayString(obj) {
	var r; 
	r = [];
	for (k in obj) {
		r.push(String(obj[k])); 
	}
	
	return r;
}


/**
 * Transform an array of object in an array of arrays
 * @param {Array} objs The array of objects to be converted to array of arrays
 * @returns {Array} An array with each value element in object into a main array
 */
function arrayObjectToArray(objs) {
	var i, r, obj;
	
	r = [];
	for (i = 0; i < objs.length; i++) {
		obj = objs[i];
		arr = objToArray(obj);
		r.push(arr); 
	}
	
	return r;
}

function arrayObjectToArrayString(objs) {
	var i, r, obj;
	
	r = [];
	for (i = 0; i < objs.length; i++) {
		obj = objs[i];
		arr = objToArrayString(obj);
		r.push(arr); 
	}
	
	return r;
}


/**
 * Get a subobject of an object
 * @param {Object} obj The object from which get the subobject
 * @param {Array} fields An array of strings with the field names  
 * @returns {Object} The subobject of the given object with only the given keys
 */
function subObject(obj, fields) {
	var r, i, v, f;
	r = {};
	for (i = 0; i < fields.length; i++) {
		f = fields[i];
		v = obj[f]; 
		if (v === undefined) {
			throw new Error("there is no key " + f + " in object: " + JSON.stringify(obj));
		} else {
			r[f] = v;
		}
	}
	
	return r;
}


/**
 * Determines if two linear objects are equals
 * @param {Object} obj1 The object 1 to compare 
 * @param {Object} obj2 The object 2 to compare
 * @returns {Boolean} True if both objects are equals, otherwise false
 */
function objectEquals(obj1, obj2) {
	var q1, q2, k1, r;
	q2 = entriesCount(obj2);
	q1 = 0;
	r = true;
	for (k1 in obj1) {
		q1++;
		r = r && (obj1[k1] == obj2[k1]);
	}
	r = r && (q1 == q2);
	return r;
}


/**
 * Get the keys of a linear object into an array  
 * @param {Object} obj The linear object in which look for the keys  
 * @returns {Array} The array of string with the key names of the linear object
 */
function objectKeys(obj) {
	var k, r;
	r = [];
	for (k in obj) {
		r.push(k); 
	}
	return r;
}


/**
 * Creates a new array with linear sobobjects of each one that compose the given array  
 * @param {Array} arr The array from which get each object to create subobjects 
 * @param {Array} keys An array with the keys that must compose each subobject in the new array
 * @returns {Array} A new array with subobjects of each one from the given array using the given keys 
 */
function subobjectArray(arr, keys) {
	var i, o, s, f, r;
	r = [];
	for (i = 0; (i < arr.length); i++) {
		o = arr[i];
		s = subObject(o, keys);
		r.push(s);
	}
	return r;
}


/**
 * Determines if exists a subobject (linear) into any linear object of the given array 
 * @param {Array} arr The array of linear objects in which look
 * @param {Object} subobj The object to look into the array of linear objects
 * @returns {Boolean} True if the subobject exists in any array entry, otherwise false
 */
function existsSubobjectInArray(arr, subobj) {
	var i, o, s, f, ok;
	ok = objectKeys(subobj);
	f = false;
	for (i = 0; ((i < arr.length) && (!f)); i++) {
		o = arr[i];
		s = subObject(o, ok);
		f = objectEquals(subobj, s);
	}
	return f;
}


/**
 * Do the union between two arrays of linear objects
 * @param {Array} arr1 The 1st array of linear objects to be joined
 * @param {Array} arr2 The 2nd array of linear objects to be joined
 * @param {Array} keys A string array with the key names that identify each element in object 1 and object 2 arrays
 */
function arrayUnion(arr1, arr2, keys) {
	var i, k, e, r, v;
	r = [];
	for (i = 0; i < arr1.length; i++) {
		v = arr1[i];
		k = subObject(v, keys);
		r.push(v);
	}
	for (i = 0; i < arr2.length; i++) {
		v = arr2[i];
		k = subObject(v, keys);
		e = existsSubobjectInArray(r, v);
		if (!e) {
			r.push(v);
		}
	}
	return r;
}


/**
 * Get the position of the first ocurrence of the given object into the given array from the given position
 * @param {Array} arr The array in which look for the object
 * @param {Object} obj The object to look into the array
 * @param {Number} pos The start position of the array to look for the object
 * @returns The position of the object into the array, or -1 of the object does not exists
 */
function arrayIndexOf(arr, obj, pos) {
	var i, v, e;
	pos = (pos)?pos:0;
	e = false;
	for (i = pos; ((i < arr.length) && (!e)); i++) {
		v = arr[i];
		e = objectEquals(v, obj);
	}
	if (!e) {
		i = -1;
	} else {
		i--;
	}
	return i;
}


/**
 * Do the minus set operation between arr1 and arr2 (arr1 - arr2) only for the given key fields of each array linear object
 * @param {Array} arr1 The array from which subtract elements
 * @param {Array} arr2 The array with the linear objects to be subtracted 
 * @param {Array} keys The subset of keys to keep from both arrays (the operation will be done with object with these fields only) 
 * @returns {Array} The minus operation between arr1 and arr2 for the given key fields (arr1 - arr2)
 */
function arrayMinus(arr1, arr2, keys) {
	var i, k, e, r, v;
	r = [];
	for (i = 0; i < arr1.length; i++) {
		v = arr1[i];
		k = keys?subObject(v, keys):v;
		r.push(k);
	}
	for (i = 0; i < arr2.length; i++) {
		v = arr2[i];
		k = keys?subObject(v, keys):v;
		p = arrayIndexOf(r, k);
		if (p > -1) {
			r.splice(p, 1);
		}
	}
	return r;
}



/**
 * Create a new object, transforming an array object to an indexed object by the given (and unique) key.<br>
 * If the object have repeated entries, the last one will remains
 * @param {Array} arr The array of linear objects to be indexed
 * @param {String} index The name of the key that will be used to index the new object 
 * @returns {Object} A new object with the given index as key, and the array components as elements.
 */
function transformToIndexedBy(arr, index) {
	var r, i, e;
	r = {};
	for (i = 0; i < arr.length; i++) {
		e = arr[i];
		r[e[index]] = e;
	}
	return r;
}


/**
 * Create a new object, transforming an array object to an indexed object by the given (and unique) key and the given field name for the value.<br>
 * If the object have repeated entries, the last one will remains
 * @param {Array} arr The array of linear objects to be indexed
 * @param {String} index The name of the key that will be used to index the new object
 * @param {String} returninField The name of the field of the linear object to set the value of the new indexed object 
 * @returns {Object} A new object with the given index as key, and the returning field name as the value
 */
function transformToValueIndexedBy(arr, index, returningField) {
	var r, i, e;
	r = {};
	for (i = 0; i < arr.length; i++) {
		e = arr[i];
		r[e[index]] = e[returningField];
	}
	return r;
}


/**
 * Create a new object, transforming an array object to an indexed object by the given (and unique) key and the given field names to make an object for the value.<br>
 * If the object have repeated entries, the last one will remains
 * @param {Array} arr The array of linear objects to be indexed
 * @param {String} index The name of the key that will be used to index the new object
 * @param {Array} returninFields The names of the fields of the linear object to compose an object as the value of the new indexed object 
 * @returns {Object} A new object with the given index as key, and an object composed by returning field names as the value
 */
function transformToObjectIndexedBy(arr, index, returningFields) {
	var r, i, e, j, k, o;
	if (typeof returningFields == "string") {
		returningFields = [returningFields];
	}
	r = {};
	for (i = 0; i < arr.length; i++) {
		e = arr[i];
		o = {};
		for (j = 0; j < returningFields.length; j++) {
			k = returningFields[j];
			o[k] = e[k]
		}
		if ((typeof index == "string") || (index.length == 1)) {
			r[e[index]] = o;
		} else if (index.length == 2) {
			if (!r[e[index[0]]]) {
				r[e[index[0]]] = {};
			}
			r[e[index[0]]][e[index[1]]] = o;
		}
	}
	return r;
}


/**
 * Create a new object, transforming an array object to an indexed object by the given (and twice) 2 keys and the given field names to make an object for the value.<br>
 * If the object have repeated entries, the last one will remains
 * @param {Array} arr The array of linear objects to be indexed
 * @param {Array} indexes An array with the names of the keys that will be used to index the new object
 * @param {Array} returninFields The names of the fields of the linear object to compose an object as the value of the new indexed object 
 * @returns {Object} A new object with the given indexes as key1 and key2 respectively, and an object composed by returning field names as the value
 */
function transformToObjectIndexedBy2(arr, indexes, returningFields) {
	var r, i, e, j, k, o;
	r = {};
	for (i = 0; i < arr.length; i++) {
		e = arr[i];
		o = {};
		for (j = 0; j < returningFields.length; j++) {
			k = returningFields[j];
			o[k] = e[k]
		}
		if (!r[e[indexes[0]]]) {
			r[e[indexes[0]]] = {};
		}
		r[e[indexes[0]]][e[indexes[1]]] = o;
	}
	return r;
}


/**
 * Get a sub array of elements that have the given keyValue into the given keyName field
 * @param {Array} array The array of linear objects in which look for the elements
 * @param {String} keyName The name of the field into each linear object to evaluate equallity 
 * @param {Object} keyValue The value to compare with the given keyName field into each linear object in the array
 * @returns {Array} An array with each element of the given array that have the field with name keyName equals to the given keyValue 
 */
function subArrayByEqualCondition(array, keyName, keyValue) {
	var retData, i, obj;
	retData = [];
	for (i = 0; i < array.length; i++) {
		obj = array[i];
		if (obj[keyName] == keyValue) {
			retData.push(obj);
		}
	}
	return retData;
}


function composeInEntries(arrObj, field) {
	var i, r, v;
	
	r = "(";
	for (i = 0; i < arrObj.length; i++) {
		v = arrObj[i][field];
		if (v instanceof String) {
			r = r + "'" + v + "',";
		} else {
			r = r + v + ",";
		}
	}
	r = r.substr(0, r.length - 1);
	r = r + ")";
	
	return r;
}


/**
 * Transform an array object to a lineal array filled only with the given field by key
 * @param arrobj The array object to transform
 * @param keyName The field name of the field used to fill the array
 * @returns An array with the given key field name for each elemento of the given array object
 */
function arrayObjToArrayByKey(arrobj, keyName) {
	var i, r;
	r = [];
	for (i = 0; i < arrobj.length; i++) {
		r = r.concat(arrobj[i][keyName]);
	}	
	return r;
}



function arrayObjectReplaceKeys(pArrayObject, pSourceKeys, pDestinationKeys) {
	var i, k, nArr, nObj, obj, nArr;
	
	nArr = [];
	for (i = 0; i < pArrayObject.length; i++) {
		obj = pArrayObject[i];
		nObj = $.extend({}, obj);
		for (k = 0; k < pSourceKeys.length; k++) {
			nObj[pDestinationKeys[k]] = obj[pSourceKeys[k]];
			delete nObj[pSourceKeys[k]];
		}
		nArr.push(nObj);
	}
	
	return nArr;
}