defaults = {
	language: "es",
	logging: true,
	backendUrl: "http://localhost/static",
	servicesUri: "/_sandbox",
	pluginsUri: "/js-plugins",
	librariesUri: "/js",
	imagesUri: "/img",
	cssUri: "/css",
	popupTitle: "¡ Atención !"
};

defaults["contextPath"] = defaults.backendUrl + "/context";
defaults["pluginsUrl"] = defaults.backendUrl + defaults.pluginsUri;
defaults["librariesUrl"] = defaults.backendUrl + defaults.librariesUri;
defaults["imagesUrl"] = defaults.backendUrl + defaults.imagesUri;
defaults["cssUrl"] = defaults.backendUrl + defaults.cssUri;


/**
 * Add to the given options a set with the basic set of options to be passed to each plugin
 * @param {Object} additionalOptions An object with the additional options to be joined to the basic ones 
 * @returns {Object} A set with the options to be passed to plugins
 */
function basicDefaults(additionalOptions) {
	var basicOptions = {
		language: defaults.language,
		logging: defaults.logging,
		backendUrl: defaults.backendUrl,
		contextPath: defaults.contextPath,
		pluginsUrl: defaults.pluginsUrl,
		pluginsUri: defaults.pluginsUri,
		librariesUri: defaults.librariesUri,
		librariesUrl: defaults.librariesUrl,
		imagesUrl: defaults.imagesUrl,
		imagesUri: defaults.imagesUri,
		cssUrl: defaults.cssUrl,
		cssUri: defaults.cssUri,
		servicesUri: defaults.servicesUri
	};
	var r = $.extend({}, basicOptions, additionalOptions);
	return r;
}


