# Example to get Cities by postal code using select2.js

## Structure
I mounted a page in the site that I have for example coding: http://codeexamples.sperrone.com.ar/test_01

The page is over codeigniter, the code is shared in BitBucket in this URL: https://bitbucket.org/saperrone/codeexamples/src

There are 2 controllers:
- 1 to show the page (a view controller): Test_01.php (https://bitbucket.org/saperrone/codeexamples/src/87780ca39ed219746acf96540d24d77ef71cf784/application/controllers/Test_01.php?at=master)
- 1 to provide data (the API): test_01_api.php (https://bitbucket.org/saperrone/codeexamples/src/87780ca39ed219746acf96540d24d77ef71cf784/application/controllers/test_01_api.php?at=master)

There are 2 model classes:
- 1 associated to the model: Test01core.php (https://bitbucket.org/saperrone/codeexamples/src/87780ca39ed219746acf96540d24d77ef71cf784/application/models/Test01core.php?at=master)
- 1 to access data (DAO): Test01dao.php (https://bitbucket.org/saperrone/codeexamples/src/87780ca39ed219746acf96540d24d77ef71cf784/application/models/Test01dao.php?at=master)

There is 1 view:
- The page to play with the example: test_01.php (https://bitbucket.org/saperrone/codeexamples/src/87780ca39ed219746acf96540d24d77ef71cf784/application/views/test_01.php?at=master)

## Explanation
I have created a MySQL table to store the data given into the CSV. 

The table was created with this SQL sentence:

``` sql
create table test_01 (
	id int auto_increment primary key,
	code_commune_insee varchar(6),
	nom_commune varchar(60),
	code_postal varchar(6),
	libelle_acheminement varchar(60),
	ligne_5 varchar(60)	
);
```

I taken the liberty to add an ID (auto-numeric and primary key), because there is no unique row identification into the CSV.

I created some indexes to speed up the DB response:

``` sql
create index idx_test_01_1 on test_01 (code_commune_insee);
create index idx_test_01_2 on test_01 (nom_commune);
create index idx_test_01_3 on test_01 (code_postal);
create index idx_test_01_4 on test_01 (libelle_acheminement);
```

The idea is solve the case like a service oriented solution, so there is a web page (accessed through the view controller), the web page have simply HTML and some JS to apply the select2.js plugin.

Then the plugin uses AJAX to consume a service that provide the cities and postal codes information, the plugin have some personalization to avoid show options when the postal code is less than 3 numbers.  
Once the typed code is equal to 3 numbers or more, calls the API using AJAX, the API do some basic validation avoiding be called without any value, the request is received by the API controller, the controller gets the filter parameter and delivers the processing to the main core class, the core class validates parameter and execute a DAO method to get the information filtered.  
So the effect in the database is minimized because the calls have a good depth level.

I created only one dropdown and no the 2 mentioned fields in the scope, because I think that the effect is the same, is more simple to code and mainly would be more clear to the user (I asked before do that). 

Once the user typed some postal code (3 or more numbers) and selects one, can press the OK button, so the selected row ID will be shown (using the classic `val()` method of jQuery), and also the whole object row (because I choose to pass all the information to the select2 plugin), using the `data` method of the select2 plugin.

> Note: is not handled the case null value, so please choose a row before press the OK button.