# Javascript jQuery plugins variation

Well, here first of all is important to differenciate the developments, if you will develop an hybrid app, then surely will be convenient to SPA pattern (single page application), but this is not the best option to develop web sites.  
Here I will show some examples with jQuery:

I use a plugin pattern in JS as a way to implement some page section solution, as well as, to implement web pages.

JS tools are stored in a js folder included in the assets folder or some root one depending on the used framework.  
JS plugins are stored in a js-plugins folder (from root or assets ones too)

Each plugin are into a folder that have an HTML, a CSS, and a JS file. This way to develop results is very good to me, because is really easy translate plugins between web sites and mobile apps, giving a real plus of maximizing the code reusing.

For example: a signin plugin will be stored in `/js-plugins/signin-1.0.0`.  
The `-1.0.0` is to handle the versioning, so the dependency includes this concept.  
Into the mentioned plugin folder will exists these files:

- **signin.css**: this have the styles needed intrinsically by the plugin
- **signin.html**: this is the HTML template (in this case a form to let the user sign in)
- **signin.js**: the JS code that handle the interaction with the backend (if needed)

here is each file:

#### `signin.css:`

``` css
.signin {}

.signin .signin-container{
	width: 60%;
	min-width: 300px;
	*width: 300px;
	padding: 20px;
	border: 1px solid;
	border-radius: 10px;
	margin: 10px auto;
	text-align: center;
}

.signin .small-logo {
	width: 200px;
	margin-bottom: 20px;
}

.signin .errors-container {
	font: 14px arial;
	color: red;
	width: 300px;
	margin: 20px auto;
}

.signin .pre-buttons-coontainer {}

.signin .buttons-coontainer {}

.signin .post-buttons-coontainer {}
```


#### `singin.html:`

``` html
<div class="jqlogin">
	<form class="form-horizontal" action="#">
		<div class="formHeader">
			<img class="small-logo">
		</div>
		<div class="form-group">
			<label for="email" class="col-sm-3 control-label email-label"></label>
			<div class="col-sm-9">
				<input type="email" class="form-control email" id="email" placeholder="">
			</div>
		</div>
		<div class="form-group">
			<label for="password" class="col-sm-3 control-label password-label"></label>
			<div class="col-sm-9">
				<input type="password" class="form-control password" id="password" placeholder="">
			</div>
		</div>
		<div class="pre-buttons-coontainer">
			<button type="button" class="btn btn-lg btn-link button-signup"></button>
		</div>
		<div class="buttons-container">
			<button type="button" class="btn btn-lg btn-primary button-signin"></button>
		</div>
		<div class="post-buttons-coontainer">
			<button type="button" class="btn btn-lg btn-link button-recoverPassword"></button>
		</div>
	</form>
</div>
```

#### `signin.js`

``` javascript
/**
 * See (http://jquery.com/). 
 * @name jQuery
 * @class
 * See the jQuery Library (http://jquery.com/) for full details. This just
 * documents the function and classes that are added to jQuery by this plug-in.
 */

/**
 * See (http://jquery.com/).
 * @name $
 * @class
 * See the jQuery Library (http://jquery.com/) for full details. This just
 * documents the function and classes that are added to jQuery by this plug-in.
 */

/**
 * See (http://jquery.com/).
 * @name fn
 * @class
 * See the jQuery Library (http://jquery.com/) for full details. This just
 * documents the function and classes that are added to jQuery by this plug-in.
 * @memberOf jQuery
 */

/**
 * @ignore
 */
requirejs(["jquery", "popup"], function($, popup) {

$.fn.signin = function(options) {
	var obj = new $.signin($(this), options);
	obj.data("signin", obj);

	return obj;
};

/**
 * signin plug-in 
 * @class
 * @constructor 
 * @author "<a href="mailto:sperrone@gmail.com.ar">Sebasti&aacute;n Perrone (sperrone@gmail.com.ar)</a>"
 * @memberOf $
 */
$.signin = function($this, options) {

	$this.options = $.extend({}, $.fn.signin.defaults, options);
	var o = $this.options;
	var l = $.extend({}, $.fn.signin.labels[o.language], o.languageKeys[o.language]);
	$this.addClass("signin");


	/**
	 * Returns the plug-in name
	 * @returns {String} The plug-in name
	 * @name pluginName
	 * @public 
	 * @inner
	 * @function 
	 * @memberOf $.signin
	 */
	$this.pluginName = function() {
		return "signin";
	};
	$this.version = "1.0.0";
	$this.pluginUri = "/" + $this.pluginName() + "-" + $this.version;


	/**
	 * Initializes the plug-in
	 */
	function init() {
		$this.load(o.pluginsUrl + $this.pluginUri + "/signin.html", function() {
			$("head").append($("<link>", {href: o.pluginsUrl + $this.pluginUri + "/signin.css", rel: "stylesheet"}));
		
			// fields and labels assignment
			email = $this.find(".email");
			password = $this.find(".password");
			confirmPassword = $this.find(".confirm-password");
			logo = $this.find(".small-logo");
			emailLabel = $this.find(".email-label");
			passwordLabel = $this.find(".password-label");
			confirmPasswordLabel = $this.find(".confirm-password-label");
			errorsContainer = $this.find(".errors-container");
			buttonsignin = $this.find(".button-reset-password");
		
		
			// translation labels
			email.attr("placeholder", l.yourEmail);
			password.attr("placeholder", l.password);
			confirmPassword.attr("placeholder", l.confirmPassword);
			emailLabel.html(l.email);
			passwordLabel.html(l.password);
			confirmPasswordLabel.html(l.confirmPassword);
			buttonsignin.html("<span class='glyphicon glyphicon-refresh'></span>&nbsp;&nbsp;" + l.signin);	
		
			logo.attr("src", o.contextPath + o.imagesUri + "/" + o.logoFileName)
		
			email.val(o.email);
			email.prop("disabled", true);
			password.val("");
			confirmPassword.val("");
			errorsContainer.contents().remove();
		
			buttonsignin.bind("click", signin);
		
			$this.changeOrientation();
		
			triggerInitialized();
		});
	}


	function triggerInitialized() {
		$this.trigger({
			type: "psichat.signin.initialized",
			message: "Signin Initialized",
			time: new Date(),
		});
	}


	/**
	 * Reset password handler 
	 */
	function signin() {
		var errors = validate();
		
		if (errors.length > 0) {
			var errorText = l.pleaseCorrectTheFollowingErrors + ":<br><br><ul><li>" + errors.join("</li><li>") + "</li></ul>";
			errorsContainer.html(errorText);
		} else {
			requestsignin();
		}
	}

	/**
	 * Validates data to reset the password
	 * @returns {Array}
	 */
	function validate() {
		var err = []; 
		if (email.val() == "") {
			err.push(l.errTypeEmail);
		}
		if (password.val() == "") {
			err.push(l.errTypePassword);
		}
		if (confirmPassword.val() == "") {
			err.push(l.errConfirmPassword);
		}
		if (confirmPassword.val() != password.val()) {
			err.push(l.errPasswordAndConfirmPasswordMustBeEqual)
		}
		
		return err;
	}


	/**
	 * Call to the service to reset the password
	 */
	function requestsignin() {
		var url = o.servicesUrl + o.servicesUri + o.signinUri;
		var data = $.param({
			hash: o.hash,
			email: o.email,
			newPassword: password.val()
		});
	
		var dataType = "json";
	
		$.ajax({
			type: "POST",
			url: url,
			data: data,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			success: responsesignin,
			error: function() {
				showErrors(arguments[0], arguments[1], arguments[2]);
			},
			dataType: dataType
		});
	}


	/**
	 * Response handler for the reset password service
	 * @param {Object} json The JSON structure devlivered by the reset password service
	 */
	function responsesignin(json) {
		$this.trigger({
			type: "iam.password.updated",
			message: "Password Updated",
			time: new Date(),
		});
	}


	/**
	 * Set an option with the given value
	 * @param {String} pOption The name of the options to change
	 * @param {Object} pValue The value to set into the option
	 */
	$this.setOption = function(pOption, pValue) {
		$this.options[pOption] = pValue;
		o[pOption] = pValue;
	};


	/**
	 * Change orientation updates
	 */
	$this.changeOrientation = function() {
		// Code here changes when switch between portrait and landscape orientation
	};


	init();

	return $this;
};


/**
 * signin defaults 
 * @name defaults
 * @memberOf $.signin
 * @class
 * @property {String} language The default language (ISO code)
 * @property {boolean} logging If true log data to console
 */
$.fn.signin.defaults = {
	language: "en",
	logging: true,
	languageKeys: {},
	contextPath: "/",
	servicesUrl: "/",
	pluginsUrl: "/",
	imagesUri: "/",
	logoFileName: "logo.jpg"
};


/**
 * signin labels
 * @name labels
 * @memberOf $.signin
 */
$.fn.signin.labels = {
	en: {
		email: "email"
	},
	es: {
		email: "email"
	}
};

$(document).trigger({
	type: "psichat.signin.ready",
	message: "Signin Ready",
	time: new Date(),
});


});
```

Looking into the JS file, you will note that the HTML is dynamically loaded, and the CSS file too. Also all the code are wrapped into the jQuery plugin pattern, each plugin receive an object with options that are usually included in the php view.  
Those are a very simple and basic way to connect jQuery plugins with CI to do a REST oriented web site.  
If the plugins is well developed, it could be used in mobile devices or also mobile hybrid developments (using cordova, phonegap, ionic, ...).


