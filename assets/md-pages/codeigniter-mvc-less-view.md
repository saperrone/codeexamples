# CodeIgniter MVC less view

## Introduction
This standard propose the following guidelines:

- remove the logic from the views: all the needed logic should be solved through some JS framework (anyone: from the low jQuery level to React, Ext JS, or any other).
- views only could use PHP to solve some session or relative URL or PATH stuff.
- controllers should solve the parameters obtaining, some really basic validation, then delegates all to a CORE class.
- CORE classes are model classes that solve some logic, do not access (directly) to database, only are dedicated to solve some bussicess core logic.
- CORE classes could interact with database through DAO classes.
- DAO classes uses the database class to do the needed operation.

## Backend - Some real examples
Here are some standards working with CodeIgniter:

I will show some classes about the status project (to handle status precedence, association to entities and status entities history).  
This development is fully oriented to REST services, so there is no views with strong logic in PHP, but lets see first the controllers...    
For each solution I usually encapsulate into a folder and a controller named api.php, in this case the folder is status. So we have `application/controllers/status/api.php`.  
I have developed a `SampleRestController` that extends from `CI_Controller`, basically this class have some code to easy the out of data and handle errors.  
Here are some methods of `status/api.php` controller:

``` php
class api extends SampleRestController {

	public function __construct() {
		parent::__construct ();
		$this->load->model('token/TokenCore');
		$this->load->model('status/StatusCore');
	}
	
	/**
	 * Start the status classification associated to an entity, a status type and an initial status
	 * @scope public
	 */
	public function start() {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
	
		// set header to handle JSON output
		header('Content-Type:application/json');
	
		// capture the necessary parameters (use GET and POST in beta and alpha versions to easy the test stuff, in production version the GET usually is removed)
		$username = $this->input->get_post("username", TRUE);
		$token = $this->input->get_post("token", TRUE);
		$entity = $this->input->get_post("entity", TRUE);
		$statusTypeCode = $this->input->get_post("statusTypeCode", TRUE);
		$initialStatusCode = $this->input->get_post("initialStatusCode", TRUE);
		$latitude = $this->input->get_post("latitude", TRUE);
		$longitude = $this->input->get_post("longitude", TRUE);
	
		// Capture the IP address for audit pusposes
		$ipAddress = $_SERVER['REMOTE_ADDR'];
		
		// logging for debug purposes (in production version the log level usually is raised, at least, to INFO)
		log_message("debug", $loc . "params (username, password, token): " . $username . ", " . $token);
		log_message("debug", $loc . "params (entity, statusTypecode, initialStatusCode): " . $entity . ", " . $statusTypeCode . ", " . $initialStatusCode);
		log_message("debug", $loc . "params (latitude, longitude): " . $latitude . ", " . $longitude);
	
		// this development use some tokenized logic to know if the call is valid
		$tokenAllowed = $this->TokenCore->tokenAllowed($username, $token);
	
		if ($tokenAllowed) {
			// the controller does not contain any logic, only capture the information and call to a method of a core class
			$r = $this->StatusCore->start($entity, $statusTypeCode, $initialStatusCode, $username, $latitude, $longitude, $ipAddress);
		} else {
			// if the token is not valid an error is shown
			$r = $this->TokenCore->createError($username);
		}
	
		// set header data to handle long JSON outputs 
		$this->setHeaderVars($r); 
	}
	
	:
	:
```

In this example, the core method do the parameter validations and contain the logic, but not the data access, for that the class invokes **DAO clases**.  
Similar to controller classes I have a `Coremodel` main class from which extends almost all classes.  
Take a look to `StatusCore.php` file (in this case placed in `application/models/status`):

``` php
class StatusCore extends Coremodel {

	public function __construct() {
		parent::__construct();
		$this->load->model("status/StatusTypeDAO");
		$this->load->model("status/StatusDAO");
		$this->load->model("status/StatusHistoryDAO");
		$this->load->model("status/StatusTransitionDAO");
		$this->load->model("status/StatusEntityDAO");
	}


	/**
	 * Starts an entity status classification
	 * @param string $entity The external entity reference 
	 * @param string $statusTypeCode The status type code
	 * @param string $initialStatusCode The status code
	 * @param string $username The username who intends to make history
	 * @param float $latitude The latitude in which the user makes history
	 * @param float $longitude The longitude in which the user makes history
	 * @param string $ipAddress The IP address from which the user makes history
	 * @return statusArray A status array structure
	 */
	public function start($entity, $statusTypeCode, $initialStatusCode, $username, $latitude, $longitude, $ipAddress) {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		// verify if status type code exists
		$statusType = $this->StatusTypeDAO->loadByCode($statusTypeCode);
		if ($statusType->num_rows() == 0) {
			$errorText = "The status type '" . $statusTypeCode . "' does not exists.";
			log_message("error", $loc . $errorText);
			$r = array(
				"status" => "401",
				"errorCode" => "StatusCore-start-001",
				"errorText" => $errorText
			);
			return $r;
		}
		log_message("debug", $loc . "status_type ok - id_status_type: " . $statusType->row()->id_status_type);

		// the status type exist
		// verify if status exists for the status type
		$status = $this->StatusDAO->loadByCode($statusTypeCode, $initialStatusCode);
		if ($status->num_rows() == 0) {
			$errorText = "The status '" . $initialStatusCode . "' does not exists for status type '" . $statusTypeCode . "'.";
			log_message("error", $loc . $errorText);
			$r = array(
				"status" => "401",
				"errorCode" => "StatusCore-start-002",
				"errorText" => $errorText
			);
			return $r;
		}	
		$idStatus = $status->row()->id_status;
		log_message("debug", $loc . "status ok - id_status: " . $idStatus);

		// the status code exists 
		// verify if the external entity reference have already started
		$history = $this->StatusHistoryDAO->loadByEntityAndType($entity, $statusTypeCode);
		if ($history->num_rows() > 0) {
			// have history then error
			$errorText = "The entity " . $entity . " have existent history, so status classification was already started.";
			log_message("error", $loc . $errorText);
			$r = array(
				"status" => "401",
				"errorCode" => "StatusCore-start-003",
				"errorText" => $errorText
			);
			return $r;
		}
		log_message("debug", $loc . "status entity history ok (is empty, so si allowed to start)");

		// does not have history 
		// all is ok to add
		$ok = $this->StatusEntityDAO->add($idStatus, $entity, $username, $ipAddress);
		if (!$ok) {
			$r = $this->createErrorDB("There was an error inserting entity status");
			return $r;
		}
		$idStatusEntity = $this->StatusEntityDAO->db->insert_id();

		$ok = $this->StatusHistoryDAO->add($entity, $idStatus, $username, $latitude, $longitude, $ipAddress);
		if (!$ok) {
			$r = $this->createErrorDB("There was an error inserting status history");
			return $r;
		}
		$idStatusHistory = $this->StatusHistoryDAO->db->insert_id();

		$r = $this->createResultOk(array(
			"id_status_history" => $idStatusHistory, 
			"id_status_entity" => $idStatusEntity
		));

		return $r;
	}

	:
	:

```

The firsts lines of the `start` method contains the parameter validation, then do some internal validation assuming that the parameters are valid, and then is the main core logic.  
If there is an error an array is used to pass information, one component is status, that is used to translate to HTTP status.  
If all is ok, then the method create the ok output (that is returned to the controller and then sent to the AJAX call). The OK response have a normalized structure too, so the Javascipt have some standards to handle answers in a centralized way.  
Taking a look to the call there are access to data that are handled by DAO (Data Access Object) clases, this classes are models but oriented to solve SQL sentences. Also could be used some ORM frameworks, some database mapping (with bean classes) logic (using a framework like Propel).  

The examples below are oriented to a simple access to data using SQL, so take a look to the `StatusHistoryDAO` class: 


``` php
class StatusHistoryDAO extends DaoModel {

	public function __construct() {
		parent::__construct();
	}


	/**
	 * Load an entity status history based on the status type code
	 * @param unknown $statusTypeCode The code of the status type
	 * @param unknown $entity The entity external reference
	 * @return QueryResult A status history query result object
	 */
	public function loadByEntityAndType($entity, $statusTypeCode) {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		$sql = "select sh.*, s.code status_code, st.code status_type_code " .
			   "from status_history sh " .
			   "inner join status s on s.id_status = sh.id_status " .
			   "inner join status_type st on st.id_status_type = s.id_status_type " .
			   "where st.code = ? " .
			   "and sh.ext_ref_entity = ? ";
		$prms = array($statusTypeCode, $entity);
		log_message('debug', $loc . "sql: " . $sql);
		log_message('debug', $loc . "prms: " . print_r($prms, true));
		$query = $this->db->query($sql, $prms);
		log_message('debug', $loc . "row_nums: " . $query->num_rows());
		return $query;
	}


	/**
	 * Add a status history entry
	 * @param string $entity The external entity reference
	 * @param string $statusTypeCode The status type code
	 * @param string $statusCode The status code
	 * @param string $username The username who intends to make history
	 * @param float $latitude The latitude in which the user makes history
	 * @param float $longitude The longitude in which the user makes history
	 * @param string $ipAddress The IP address from which the user makes history
	 * @return boolean True if the insert works fine otherwise false
	 */
	public function add($entity, $idStatus, $username, $latitude, $longitude, $ipAddress) {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		log_message("debug", $loc . "params: $entity, $idStatus, $username, $latitude, $longitude, $ipAddress");
		$row = array(
			"ext_ref_entity" => $entity,
			"id_status" => $idStatus,
			"status_date" => date("Y-m-d H:i:s"),
			"status_user" => $username,
			"status_longitude" => $longitude,
			"status_latitude" => $latitude,
			"add_date" => date("Y-m-d H:i:s"),
			"add_user" => $username,
			"add_terminal" => $ipAddress,
		);
		$r = $this->db->insert("status_history", $row);
		log_message("debug", $loc . "r: " . print_r($r, true));
		return $r;
	}

	:
	:

```

So the DAO methods are simple SQL (but simetimes are not so simple ![](/assets/img/wink.png) ), or simple function to create, update or delete data. The idea is satisfy the main CRUD functions over a table.  
This examples are very simple, are not using cache concepts or session cached data, that sometimes are pretty necessary because performace issues.  
Also there is no use of ORM frameworks, in consequence there aren't exists entity classes (representations of the database structures).

## Frontend - About views (with less PHP) 
There are some adaptations needed to handle SESSION variables with Javascript, so the view classes could have some Javascript sections merged with PHP to reach SESSION data.

Here is a simple view:

``` php
<?php
$this->load->view("publicHeader"); 
?>

<h1>PsiChat</h1>
<div class="signin"></div>

<script type="text/javascript">
	requirejs(["jquery", "signin"], function($, signin) {
		$(document).bind("psichat.signin.ready", function(ev) {
			$(".signin").signin(basicDefaults());
		});
	});
</script>

<?php
$this->load->view("footer"); 
?>
```

The public header view contains the main inclussions and, in this case, the header for public accessed pages:

``` php
<?php 
$this->load->view("basicJsInclude");
?>
requirejs(["jquery"], function($) {
	$(document).ready(function() {
		$("html").attr("lang", defaults.language);
		$("html").find("title").html(lk.welcomeToPsiChat);
	});
});

</script>
```

And here is the basicJsInclude file:

``` php
<?php
$this->load->helper('url'); 
?>

<script type="text/javascript" src="<?php echo base_url("");?>/js/require-2.1.22.js"></script>
<script type="text/javascript" src="<?php echo base_url("");?>/js/defaults-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url("");?>/js/language-keys/language-keys-es-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url("");?>/js/language-keys/language-keys-en-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url("");?>/js/global-1.0.0.js"></script>
<script type="text/javascript">
	defaults.contextPath = "<?php echo base_url("");?>";
	defaults.backendUrl = "<?php echo base_url("");?>" + defaults.servicesUri;
	defaults.pluginsUrl = "<?php echo base_url("");?>" + defaults.pluginsUri;
	defaults.imagesUrl = "<?php echo base_url("");?>" + defaults.imagesUri;
	defaults.cssUrl = "<?php echo base_url("");?>" + defaults.cssUri;
</script>

<link href="<?php echo base_url("");?>/css/bootstrap-3.3.6/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url("");?>/css/bootstrap-3.3.6/bootstrap-theme.css" rel="stylesheet">

<script type="text/javascript">

	requirejs.config({
		baseUrl: defaults.contextPath + "js",
		paths: {
			bootstrap: "bootstrap-3.3.6",
			moment: "moment/moment-2.11.2",
			numeral: "numeral/numeral-1.5.3",
			numeralLanguages: "numeral/languages-1.5.3",
			jsonjs: "jsonjs/json2-2015-05-03",
			magnificPopup: "magnific-popup/jquery.magnific-popup-1.0.1",
			jquery: "jquery-2.1.4",
			popup: defaults.pluginsUrl + "/popup-1.0.0/popup",
			signin: defaults.pluginsUrl + "/signin-1.0.0/signin"
		}
	});
	
	lk = languageKeys[defaults.language];

	requirejs(["jquery"], function($) {
		$.ajaxSetup({
			cache: false
		});
	});
	
	:
	:
```

Take a look to this section:

``` php
<script type="text/javascript">
	defaults.contextPath = "<?php echo base_url("");?>";
	defaults.backendUrl = "<?php echo base_url("");?>" + defaults.servicesUri;
	defaults.pluginsUrl = "<?php echo base_url("");?>" + defaults.pluginsUri;
	defaults.imagesUrl = "<?php echo base_url("");?>" + defaults.imagesUri;
	defaults.cssUrl = "<?php echo base_url("");?>" + defaults.cssUri;
</script>
```

This is what I mean when I mentioned merged JS and PHP code, the same techinque is used when the page needs some SESSION variables.


> Well, this basically a very simple and success standard that I have implemented over 5 complex projects...
