# Coding standards

This repo is to show some different code standards with examples.

## CodeIgniter MVC, less view variant

- This standard use the primary MVC proposal
- Have DAOs (Data Access Objects) into the model class set
- Have core classes to handle the main logic into model classes set too
- Have controllers to receive submits, do a simple pre-validation and delegates to model classes
- The views are mostly simple HTML (except tohandle session variables) with some strong JS framework with AJAX calls

[see a more detailed explanation...](/CodeIgniterLessView)

## jQuery plugin pattern variation

- encapsulates logic into plugins
- each plugin have a CSS, a HTML and a JS file
- HTML file is the HTML template
- CSS file is the styling needed for the plugin
- JS file have the logic
- these plugins are cohesive and autocontained with a high reusability

[see a more detailed explanation...](/JQueryPluginVariation)

## select2.js example using AJAX with an API accesing MySQL

This is an example showing how to use select2.js plugin to filter cities by postal code, and then access the ID of the row and the whole object row.

[see a more detailed explanation...](/select2ajaxexample)