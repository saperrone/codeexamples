# Coding standards

This repo is to show some different code standards with examples.

## CodeIgniter MVC, less view variant

- This standard use the primary MVC proposal
- Have DAOs (Data Access Objects) into the model class set
- Have core classes to handle the main logic into model classes set too
- Have controllers to receive submits, do a simple pre-validation and delegates to model classes
- The views are mostly simple HTML (except tohandle session variables) with some strong JS framework with AJAX calls

## jQuery plugin pattern variation

- encapsulates logic into plugins
- each plugin have a CSS, a HTML and a JS file
- HTML file is the HTML template
- CSS file is the styling needed for the plugin
- JS file have the logic
- these plugins are cohesive and autocontained with a high reusability

