<?php

require_once(APPPATH . '/libraries/SampleRestController.php');

/**
 * @author      '<a href=mailto:sperrone@gmail.com.ar>Sebasti&aacute;n Perrone (sperrone@gmail.com.ar)</a>'
 * @category    Controller
 * @since 		02-08-2017
 * 
 * The main controller for cities information
 */
class Test_01_api extends SampleRestController {
	
	/**
	 * Contructor for cities information
	 * @scope public
	 */
	public function __construct() {
		parent::__construct ();
		$this->load->model("Test01core");
	}
	
	/**
	 * Get city information by postal code
	 * @scope public
	 */
	public function postalcodes() {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		log_message("debug", $loc . "start");
	
		// Getting the sign up data
		$fiter = $this->input->get_post("filter", TRUE);
		$response = $this->Test01core->getByPostalCode($fiter);
		$this->setHeaderVars($response);
	}
}


?>