<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CodeIgniterLessView extends CI_Controller {

	public function index()
	{
		$this->load->view('codeigniter-lessview');
	}
}