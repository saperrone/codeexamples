<?php 

require_once(APPPATH . '/libraries/DaoModel.php');

/**
 * @author     '<a href=mailto:sperrone@gmail.com.ar>Sebasti&aacute;n Perrone (sperrone@gmail.com.ar)</a>'
 * @category   Model
 * @since      20-04-2016
 * 
 * The DAO model for user session data utilities 
 */
class Sessiondao extends DaoModel {
  
	public function __construct() {
		parent::__construct();
	}


	public function loadByIdSession($pIdSession) {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		$sql = "select ses.* " .
				"from ci_sessions ses " .
				"where ses.id = ? ";
		$prms = array($pIdSession);
		log_message("debug", $loc . "sql: " . $sql);
		log_message("debug", $loc . "prm: " . print_r($prms, true));
		$query = $this->db->query($sql, $prms);
		log_message("debug", $loc . "num_rows : " . $query->num_rows());
		return $query;
	}

}
