<?php 

require_once(APPPATH . '/libraries/DaoModel.php');

/**
 * @author     '<a href=mailto:sperrone@gmail.com.ar>Sebasti&aacute;n Perrone (sperrone@gmail.com.ar)</a>'
 * @category   Model
 * @since      2017-08-02
 * 
 * The DAO model for cities and postal codes
 */
class Test01dao extends DaoModel {
  
	public function __construct() {
		parent::__construct();
	}


	/**
	 * Load city information by postal code
	 * @param string $username The username of the user
	 * @return queryResult A query result with the user object
	 */
	public function loadByPostalCode($pPostalCode) {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		$sql = "select * " .
				"from test_01 u " .
				"where u.code_postal like ? ";
		$prms = array($pPostalCode . "%");
		log_message("debug", $loc . "sql: " . $sql);
		log_message("debug", $loc . "prm: " . print_r($prms, true));
		$query = $this->db->query($sql, $prms);
		log_message("debug", $loc . "num_rows : " . $query->num_rows());
		return $query;
	}
	
}

?>