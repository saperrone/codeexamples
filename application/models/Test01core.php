<?php 

/**
 * @author      '<a href=mailto:sperrone@gmail.com.ar>Sebasti&aacute;n Perrone (sperrone@gmail.com.ar)</a>'
 * @category    Core Model
 * @since 		02-08-2017
 * The base model for the core cities management
 */
require_once(APPPATH . '/libraries/CoreModel.php');
class Test01core extends CoreModel {

	const ERR_001 = "Test01Core-001"; // postal code must be informed
	
	public function __construct() {
		parent::__construct();
		$this->load->model("Test01dao");
	}
	

	/**
	 * Get city information based on the postal code
	 * @param string $postalCode the postal code (or the first letters of one, searchs with SQL like operator)
	 * @return resultService A service result with a list of cities
	 */
	public function getByPostalcode($postalCode) {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		log_message("debug", $loc . "parameters: " . $postalCode);

		if (!$postalCode) {
			$errorText = "the postal must be informed";
			$r = $this->createCoreError(self::ERR_001, $errorText);
			return $r;
		}
		
		$citiesQuery = $this->Test01dao->loadByPostalCode($postalCode);
		return $this->createResultOk($citiesQuery->result());
	}
	
}
