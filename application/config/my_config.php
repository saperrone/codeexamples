<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| SYSTEM GENERAL VARIABLES
|--------------------------------------------------------------------------
|
*/
$config["system_name"]	= "CodeExamples";
$config["default_language"]	= "es";

/*
|--------------------------------------------------------------------------
| USER SECURITY CONFIGURATION
|--------------------------------------------------------------------------
|
*/
$config["hash_salt"] = "<salt>";
$config["user_file_upload_path"] = "/var/www/html/opendistrict/<some_dir>";
$config["user_max_additional_data_files"] = 5; 

/*
|--------------------------------------------------------------------------
| TIME ZONE SETTING
|--------------------------------------------------------------------------
|
*/
$config['time_zone']	= 'America/Argentina/Buenos_Aires';


/*
|--------------------------------------------------------------------------
| EMAIL SENDING LOGIN CREDENTIALS  
|--------------------------------------------------------------------------
|
*/

$config['protocol']     = 'smtp'; 						//Email Protocol
$config['smtp_host']    = 'ssl://smtp.gmail.com'; 		//SMTP Host
$config['smtp_port']    = 465; 							//SMTP Port
$config['smtp_user']    = 'sperrone.test1@gmail.com'; 	//SMTP username
$config['smtp_pass']    = '<passwd>'; 					//SMTP Password
$config['mailtype']     = 'html'; 						//Mail Type
$config['from']         = 'noreply@<somedomain.com.ar>'; 	//From Address
//$config['proxy']        = 'proxyserverHost:proxyPort';                 //Network proxy


/*
 |--------------------------------------------------------------------------
 | PERIODIC TASKS CONFIGURATION
 |--------------------------------------------------------------------------
 |
 */
$config['some_periodic_task_key'] = 10;


/*
 |--------------------------------------------------------------------------
 | ADMIN CONTACT INFORMATION
 |--------------------------------------------------------------------------
 |
 */
$config['admin_email']     = '<admin@somedomain.com.ar>'; 	//Administrator email
$config['admin_phones']     = '(11) 5555-5555'; 						//Administrator phones
$config['admin_address']     = 'some place 10 - some dept - some neighborhood<br>' .
                              'some locality - some city - some state'; // Administrator address 						
