<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
/**
 * @author     '<a href=mailto:sperrone@gmail.com.ar>Sebasti&aacute;n Perrone (sperrone@gmail.com.ar)</a>'
 * @category   Model
 * @since      11-08-2015
 * 
 * The base class ro create DAO models 
 */
class DaoModel extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * Get the directory of the current class
	 * @return string The directory of the current class (from the Code Igniter application dir)
	 */
	protected function getDirectory() {
		$r = substr(__DIR__, strpos(__DIR__, "application") + 12, 200);
		return $r;
	}

	
	/**
	 * Create an insert response structure
	 * @param boolean $boolSuccess Indicates if the insert was successfully (true) or not (false)
	 * @param string $logMessage The suffix message to add to success or failed into structure
	 * @return array An insert response structure with the given data
	 */
	protected function createInsertResponse($boolSuccess, $logMessage) {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		if ($boolSuccess == true) {
			log_message('debug', $loc . "Success " . $logMessage);
			$r = array(
				"id" => $this->db->insert_id(),
				"errorCode" => null,
				"errorText" => null,
				"success" => true
			);
		} else {
			log_message('error', $loc . "Failed " . $logMessage);
			$r = array(
				"id" => 0,
				"errorCode" => $this->db->_error_code(),
				"errorText" => $this->db->_error_text(),
				"success" => false
			);
		}
		return $r;
	}

	
	/**
	 * Create an update response structure
	 * @param boolean $boolSuccess Indicates if the insert was successfully (true) or not (false)
	 * @param string $logMessage The suffix message to add to success or failed into structure
	 * @return array An insert response structure with the given data
	 */
	protected function createUpdateResponse($boolSuccess, $logMessage) {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		if ($boolSuccess == true) {
			log_message('debug', $loc . "Success " . $logMessage);
			$r = array(
					"affectedRows" => $this->db->affected_rows(),
					"errorCode" => null,
					"errorText" => null,
					"success" => true
			);
		} else {
			log_message('error', $loc . "Failed " . $logMessage);
			$r = array(
					"affectedRows" => 0,
					"errorCode" => $this->db->_error_code(),
					"errorText" => $this->db->_error_text(),
					"success" => false
			);
		}
		return $r;
	}


	/**
	 * Add addition audit field
	 * @param array $insertArray Array in which to add addition audit fields
	 * @param string $username The username of the user
	 * @return array The updated array with the addition audit fields
	 */
	protected function addAuditFieldsToAdd($insertArray, $username) {
		$auditArray = array(
				"add_date" => date("Y-m-d H:i:s"),
				"add_user" => $username,
				"add_terminal" => $_SERVER["SERVER_ADDR"]
		);
	
		$finalArray = array_merge($insertArray, $auditArray);
		return $finalArray;
	}
	
	
	/**
	 * Add actualization audit field
	 * @param array $actualizeArray Array in which to add actualization audit fields
	 * @param string $username The username of the user
	 * @return array The updated array with the actualization audit fields
	 */
	protected function addAuditFieldsToAct($actualizeArray, $username) {
		$auditArray = array(
				"upd_date" => date("Y-m-d H:i:s"),
				"upd_user" => $username,
				"upd_terminal" => $_SERVER["SERVER_ADDR"]
		);
	
		$finalArray = array_merge($actualizeArray, $auditArray);
		return $finalArray;
	}
	
	
}