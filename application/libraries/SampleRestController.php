<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author   '<a href=mailto:sperrone@gmail.com.ar>Sebasti&aacute;n Perrone (sperrone@gmail.com.ar)</a>'
 * @category Controller
 * @since    18-08-2015
 *
 * A sample class to manage JSON rest API controllers
 */
class SampleRestController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Sessiondao");
	}

	
	/**
	 * Get the directory of the current class
	 * @return string The directory of the current class (from the Code Igniter application dir)
	 */
	protected function getDirectory() {
		$r = substr(__DIR__, strpos(__DIR__, "application") + 12, 200);
		return $r;
	}

	
	/**
	 * Set content variables
	 * @param integer $status The http code to respond
	 */
	protected function setHeaderVars($response) {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		
		$this->output->set_status_header($response["status"]);
		$this->output->set_content_type("application/json");
		$json = json_encode($response);
		$this->output->set_header('Content-Length: ' . strlen($json));
		log_message('debug', $loc . "output length: " . strlen($json));
		$this->output->set_output($json);
		
	}

	
	/**
	 * Avoid access to default controller
	 */
	public function index() {
		$r = array(
			"status" => 403,
			"errorText" => "This controller have no default function"
		);
		return $this->setHeaderVars($r);
	}
	
	
	/**
     * Returns true if a session is valid, otherwise false
     */
	protected function sessionValid() {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		log_message("debug", "session control started");
		
		$cookHeader = $this->input->get_request_header("cookie");  
		$cookNameConfig = $salt = $this->config->item("sess_cookie_name");
		$posCook = strpos($cookHeader, $cookNameConfig);
		log_message("debug", $loc . "cookie header: '" . $cookHeader . "', cookie name in config: '" . $cookNameConfig . 
			"', pos name in header cookie: " . $posCook);
		
		if ($posCook === FALSE) {
			log_message("debug", $loc . "no session name in header ==> session invalid");
			return false;
		}
		
		$cookClean = substr($cookHeader, (strlen($cookNameConfig) + 1));
		log_message("debug", $loc . "session ID from header: '" . $cookClean . "'");
		
		$sesQuery = $this->Sessiondao->loadByIdSession($cookClean);
		if ($sesQuery->num_rows() == 0) {
			log_message("debug", $loc . "no session stored in DB ==> session invalid");
			return false;
		}
		
		log_message("debug", $loc . "session ok");
		return true;
	}

	
	protected function createInvalidSessionError($pErrorCode) {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		
		$errorText = "This function needs a valid session";
		log_message("error", $loc . "error: " . $errorText);
		$r = array(
			"status" => "400",
			"errorCode" => $pErrorCode,
			"errorText" => $errorText
		);
		return $r;
	}
}