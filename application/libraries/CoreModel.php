<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
/**
 * @author   '<a href=mailto:sperrone@gmail.com.ar>Sebasti&aacute;n Perrone (sperrone@gmail.com.ar)</a>'
 * @category Controller
 * @since    13-08-2015
 *
 * A sample class to manage the core logic for service layer
 */
class CoreModel extends CI_Model {
	
	public $words = array(
		"es" => array(),
		"en" => array()
	);
	
	public $w;
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$lang = $this->getLanguage();
		log_message("debug", "language: " . $lang);
		$this->w = $this->words[$lang];
	}
	
	/**
	 * Get the directory of the current class
	 * @return string The directory of the current class (from the Code Igniter application dir)
	 */
	protected function getDirectory() {
		$r = substr(__DIR__, strpos(__DIR__, "application") + 12, 200);
		return $r;
	}

	
	/**
	 * Create a database error message
	 * @param string $message The message to prefix the database error
	 * @return string A message with the database error code and error message
	 */
	protected function createDbError($message) {
		$err = $message . ": (" . $this->db->_error_code() . ") " . $this->db->_error_text();
		return $err;
	}

	
	/**
	 * Create a normalized core error structure
	 * @param string $pErrorCode The error code
	 * @param string $pErrorText The error text
	 * @param array $pAdditinalData An array with additional data (merged in the error array)
	 * @return array An array with the normalized error structure
	 */
	protected function createCoreError($pErrorCode, $pErrorText, $pAdditinalData = null) {
		$loc = $this->getDirectory() . "/" . __METHOD__ . " - ";
		log_message("error", $loc . "error: " . $pErrorText);
		$r = array(
			"status" => "400",
			"errorCode" => $pErrorCode,
			"errorText" => $pErrorText
		);
		if ($pAdditinalData) {
			$r = array_merge($pAdditinalData, $r);
		}
		return $r;
	}
	
	/**
	 * Create a success core response structure
	 * @param array $data The data to be embedded into the response
	 * @return array A success array structure 
	 */
	protected function createResultOk($data) {
		$r = array(
			"status" => 200,
			"data" => $data
		);
		return $r;
	}
	
	
	protected function getLanguage() {
		$sessionLang = $this->session->userdata("language");
		if (!$sessionLang) {
			$sessionLang = $this->config->item("default_language");
		}
		return $sessionLang;
	}

	
	/**
	 * Get the IP address of the accssing computer
	 * @return string
	 */
	protected function getClientIp() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	
}