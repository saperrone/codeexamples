<?php

//require_once('CIDatabaseTestCase.php');


/**
 * Token System Api Test Cases 
 *
 * @author     Sebastian Perrone (sperrone@gmail.com)
 */
class ApiSystem_test extends DbTestCase {
	
	private function deleteTestData($code) {
		$this->db->query("delete from token_system where code = ?", array($code));
	}
	
	public function testCreate() {
		echo("\n\n");
		$code = "testSys2";
		$name = "name sys 2";
		
		$this->deleteTestData($code);
		$this->request('POST', 'token/api/system/create', array("code" => $code, "name" => $name));
		$o = $this->CI->output->get_output();
		echo("====> response: " . print_r($o, true) . "\n");
		$jsonOut = json_decode($o, true);
		echo("====> response decoded to array: " . print_r($jsonOut, true) . "\n");
		$this->assertEquals(1, $jsonOut["data"]["insertOk"]);
		$this->assertGreaterThan(0, $jsonOut["data"]["id"]);
		
		$q = $this->CI->db->query("select * from token_system where code = ?", array($code));
		$this->assertEquals($name, $q->row()->name);
		
		echo("====> finish\n");
	}

// 	public function test_method_404() {
// 		$this->request('GET', 'welcome/method_not_exist');
// 		$this->assertResponseCode(404);
// 	}

// 	public function test_APPPATH() {
// 		$actual = realpath(APPPATH);
// 		$expected = realpath(__DIR__ . '/../..');
// 		$this->assertEquals(
// 			$expected,
// 			$actual,
// 			'Your APPPATH seems to be wrong. Check your $application_folder in tests/Bootstrap.php'
// 		);
// 	}

}
