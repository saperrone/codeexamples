<?php
class TokenCore_test extends DbTestCase
{

	public function setUp() {
		$this->obj = $this->newModel('token/Tokencore');
	}

	
	private function deleteTestData($code) {
		$this->CI->db->query("delete from token_system where code = ?", array($code));
	}
	
	public function test_get_category_list() {
		$code = "testSys1";
		$name = "name sys 1";
		$this->deleteTestData($code);
		$res = $this->obj->createSystem($code, $name);
		print_r($res);
		print("\n");
		$this->assertEquals(true, $res["data"]["insertOk"]);
		
		$q = $this->CI->db->query("select * from token_system where code = ?", array($code));
		$this->assertEquals($name, $q->row()->name);
		$this->deleteTestData($code);
	}
	
}