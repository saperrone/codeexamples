<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang["confirmSignup"] = "Confirmar registro";
$lang["hi"] = "Hola";
$lang["requestNewUser"] = "Una solicitud para agregarlo como nuevo usuario nos fue enviada";
$lang["signupConfirmation"] = "Confirmación de registro de usuario";
$lang["thankYou"] = "Muchas gracias";
$lang["theTeam"] = "El equipo";
$lang["toConfirmClick"] = "Para confirmarlo, por favor, haga clic en el siguiente vínculo";
