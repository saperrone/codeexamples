<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Email labels
$lang["confirmSignup"] = "Confirm signup";
$lang["hi"] = "Hi";
$lang["requestNewUser"] = "A request to add you as a new user was sent to us";
$lang["signupConfirmation"] = "User signup confirmation";
$lang["thankYou"] = "Thank you";
$lang["theTeam"] = "The team";
$lang["toConfirmClick"] = "To confirm that please click on the following link";

