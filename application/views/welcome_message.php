<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/libraries/Parsedown.php');
require_once(APPPATH . '/libraries/ParsedownExtra.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>

<link type="text/css" rel="stylesheet" href="<?php base_url()?>/assets/css/bootstrap-3.3.6/bootstrap.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="<?php base_url()?>/assets/css/prism.css" rel="stylesheet"> 
<meta charset="utf-8">
<title>Code Examples - Sebastian Alejandro Perrone</title>

<style type="text/css">
.main-container {
	margin: 10px 20px;
}
h1, h2, h3, h4, h5, h6 {
	zoom: .65;
}
</style>

</head>
<body>

<div class="main-container">
	
<?php
$md_content = file_get_contents(APPPATH . '/../assets/md-pages/readme.md', FILE_USE_INCLUDE_PATH);
$parsedown = new Parsedown();
echo $parsedown->text($md_content);
?>
	
</div>

</body>
</html>