<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>

<link type="text/css" rel="stylesheet" href="<?php base_url()?>/assets/css/bootstrap-3.3.6/bootstrap.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="<?php base_url()?>/assets/js/select2-4.0.3/select2.css" rel="stylesheet">
<script type="text/javascript" src="<?php base_url()?>/assets/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="<?php base_url()?>/assets/js/select2-4.0.3/select2.full.js"></script>
<script type="text/javascript" src="<?php base_url()?>/assets/js/generic-select2-1.0.0/generic-select2.js"></script>
<script type="text/javascript" src="<?php base_url()?>/assets/js/json2-2015-05-03.js"></script>

<meta charset="utf-8">
<title>Code Examples - Select2 use with AJAX and MySQL - Sebastian Alejandro Perrone</title>

<style type="text/css">
.test-01-form {
	width: 500px;
	margin: 50px auto 0 auto;
	text-align: center;
	padding: 10px;
	border: 1px solid;
}

.btn-ok {
	margin-top: 50px;
}

.result {
	padding: 10px;
	margin-top: 20px;
	text-align: left;
}
</style>

</head>
<body>

<div class="main-container">

<form action="#" class="test-01-form">
	<h4>Test 01 - select2 with AJAX and MySQL</h4>
	<select class="form-control test-01-select"></select>
	<button type="submit" class="btn btn-primary btn-ok">OK</button>
	<div class="result"></div>
</form>
	
</div>

<script type="text/javascript">
$(document).ready(function() {
	var sel = $(".test-01-select");
	var res = $(".result");
	var selPlugin = sel.select2({
		placeholder: 'Select an option',
		ajax: {
			data: function (params) {
				var query = {
					filter: params.term
				}

				// Query paramters will be ?search=[term]&page=[page]
				return query;
			},
			type: "GET",
			dataType: "json",
			url: <?php base_url()?>'/test_01_api/postalcodes',
			processResults: function (json) {
				return {
					results: json.data
				};
			}
		},
		id: function(city){ return city.id; },
		escapeMarkup: function (markup) { return markup; },
		minimumInputLength: 3,
		templateSelection: function (city) {
			if (city.id) {
				return city.nom_commune + "(" + city.code_postal + ")";
			} else {
				return city.text;
			}
	    },
	    templateResult: function (city) {
		    if (city.loading) return city.code_postal;
			markup = "<div>" + city.code_postal + " - " + city.nom_commune + "</div>";
		    return markup;
		}
	});

	$(".test-01-form").bind("submit", function(ev) {
		var v = selPlugin.val();
		var o = selPlugin.select2("data")[0];
		delete o.element;
		delete o.text;
		delete o.selected;
		delete o._resultId;
		delete o.disabled;
		res.html("Selected ID (using jquery .val() method): " + v + "<br><hr>" +
				 "this is the whole selected object: <pre>" + JSON.stringify(o, null, 2) + "</pre>");   
		return false;
	});
});
</script>

</body>
</html>