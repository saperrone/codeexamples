use codeexamples;

CREATE TABLE ci_sessions (
  id varchar(40) NOT NULL DEFAULT '0',
  ip_address varchar(45) NOT NULL DEFAULT '0',
  timestamp int(10) unsigned NOT NULL DEFAULT '0',
  data text NOT NULL,
  PRIMARY KEY (id)
) 
ENGINE=InnoDB 
DEFAULT CHARSET=utf8;
